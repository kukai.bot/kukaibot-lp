# 8i9.me Landing Page

```
yarn
yarn build
```

and check `dist/`

## dev

Rewrite `webpack.config.js`'s `mode` to `development`

```
yarn
yarn serve
```

and check `localhost:8080`

(c) 2021 Kukai Bot Team, Cutls All rights reserved: [No License](https://choosealicense.com/no-permission/)